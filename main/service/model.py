from sentence_transformers import SentenceTransformer, models
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
import torch


class Model:

    @staticmethod
    def model_inilization():
        global model,embeddings,testcases,testcasesid
        word_embedding_model = models.Transformer('main\models\model1', max_seq_length=512)
        pooling_model = models.Pooling(word_embedding_model.get_word_embedding_dimension())
        model = SentenceTransformer(modules=[word_embedding_model, pooling_model])

        df = pd.read_excel('main\data\BSP-CASA_TestDesign_v1.0.xlsx',4)
        testcases = df['Test Case Description'].dropna()
        testcasesid = df['Test Case ID'].dropna().to_list()
        embeddings = model.encode(testcases.to_list(), show_progress_bar=True)
        return 'created'
    @staticmethod
    def doc_vec(doc):
        doc_vec=model.encode(doc)
        return doc_vec

    @staticmethod
    def model_search(doc,topn=15):
        vec = Model.doc_vec(doc)
        probalilitys = cosine_similarity(embeddings,[vec])
        top_prob = probalilitys.flatten().argsort()[-topn:][::-1]
        doc = []
        for i,j in enumerate(top_prob):
            doc.append({'test_case_id': str(testcasesid[j]),'desc':testcases[j][0],'relevance':j})

        return doc


            

        

        

        




