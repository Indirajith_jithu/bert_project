
from sklearn.metrics.pairwise import cosine_similarity
from main.service.filedbservie import FileDbService
from main.service.modelservice import ModelService

class SearchService:

    @staticmethod
    def compare(query_vec,embedded,topn):
        relavences =  cosine_similarity([query_vec],embedded)[0]
        topn_indexs = relavences.argsort()[-topn:][::-1]
        return topn_indexs, relavences[topn_indexs]


    @staticmethod
    def get_result(Objectsids,relavences):
        myquery = { "_id": { "$in": Objectsids } }
        filters = {"_id": 0, "testcase_desc": 1,"testcase_id":1}
        curser =FileDbService.get_db_embeddings(filters,myquery)
        result = []
        for i,testcase in enumerate(curser):
            testcase['relavence']=  round(relavences[i], 5)
            result.append(testcase)

        return result    


    @staticmethod
    def search(query,topn=10):
        query_vec = ModelService.get_embeddings(query)
        embeddings = []
        Objectsids = []
        for curser in FileDbService.get_db_embeddings(filters = {"_id": 1, "embedding": 1}):
             embeddings.append(curser['embedding'])
             Objectsids.append(curser['_id'])
        topn_relavences_idx,relavences =  SearchService.compare(query_vec,embeddings,topn)
        objectidslist = [Objectsids[x] for x in topn_relavences_idx]
        return SearchService.get_result(objectidslist,relavences)
