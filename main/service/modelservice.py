from sentence_transformers import SentenceTransformer, InputExample, util,\
                                  datasets, evaluation, losses,models
from torch.utils.data import DataLoader
import os
from main.service.filedbservie import FileDbService
#from sklearn.metrics.pairwise import cosine_similarity

custom_model_path = 'main/models/custom_model'
base_model_path = 'main/models/base_model/bert_base_uncased'

class ModelService:

    

    @staticmethod
    def load_model(path):
        if path == custom_model_path:
            model = SentenceTransformer(path)
            
        else :
            word_embedding_model = models.Transformer(path)
            if path == "bert-base-uncased":
                word_embedding_model.save(base_model_path)
            pooling_model = models.Pooling(word_embedding_model.get_word_embedding_dimension(),'cls')
            model = SentenceTransformer(modules=[word_embedding_model, pooling_model])

            
        return model
    
    @staticmethod
    def get_custom_model(testcase_desc,path):
     
        print('enterefd')
        train_dataset = datasets.DenoisingAutoEncoderDataset(testcase_desc)
        train_dataloader = DataLoader(train_dataset, batch_size=10, shuffle=True)

        model = ModelService.load_model(path)       
        
        train_loss = losses.DenoisingAutoEncoderLoss(model, decoder_name_or_path=base_model_path, tie_encoder_decoder=True)
        model.fit(
            train_objectives=[(train_dataloader, train_loss)],
            epochs=1,
            weight_decay=0,
            scheduler='constantlr',
            optimizer_params={'lr': 3e-5},
            show_progress_bar=True
        )
        model.save(custom_model_path)
        return model




    @staticmethod
    def get_embeddings(testcase_desc,update_model=None):
        if os.listdir(custom_model_path):
            path = custom_model_path
        else:
            path = base_model_path
            if not os.listdir(path):
                path = "bert-base-uncased"
        if  update_model:   
            model = ModelService.get_custom_model(testcase_desc= testcase_desc,path = path)
        else :
            model = ModelService.load_model(path)

        return model.encode(testcase_desc, show_progress_bar=True)

    # @staticmethod
    # def get_embedding(query):
    #     if os.listdir(custom_model_path):
    #         path = custom_model_path
    #     else:
    #         path = base_model_path
    #     model = ModelService.load_model(path)
    #     return model.encode(query)


    # @staticmethod
    # def compare(query_vec,embedded,topn):
    #     relavences =  cosine_similarity([query_vec],embedded)[0]
    #     topn_indexs = relavences.argsort()[-topn:][::-1]
    #     return topn_indexs, relavences[topn_indexs]

    # @staticmethod
    # def get_result(Objectsids,relavences):
    #     myquery = { "_id": { "$in": Objectsids } }
    #     filters = {"_id": 0, "testcase_desc": 1,'testcase_id':1}
    #     curser =FileDbService.get_db_embeddings(myquery, )
    #     result = []
    #     for i,tescase in enumerate(curser):
    #         tescase['relavence']=  round(relavences[i], 2)
    #         result.append(tescase)

    #     return result    




    # @staticmethod
    # def search(query,topn=10):
    #     query_vec = ModelService.get_embedding(query)
    #     embeddings = []
    #     Objectsids = []
    #     for curser in FileDbService.get_db_embeddings(filters = {"_id": 1, "embedding": 1}):
    #          embeddings.append(curser['embedding'])
    #          Objectsids.append(curser['_id'])
    #     topn_relavences_idx,relavences =  ModelService.compare(query_vec,embeddings,topn)
    #     objectidslist = [Objectsids[x] for x in topn_relavences_idx]
    #     return ModelService.get_result(objectidslist,relavences)

    
    '''@staticmethod
    def model_inilization():
        global model,embeddings,testcases,testcasesid
        word_embedding_model = models.Transformer('main\models\model1', max_seq_length=512)
        pooling_model = models.Pooling(word_embedding_model.get_word_embedding_dimension())
        model = SentenceTransformer(modules=[word_embedding_model, pooling_model])

        df = pd.read_excel('main\data\BSP-CASA_TestDesign_v1.0.xlsx',4)
        testcases = df['Test Case Description'].dropna()
        testcasesid = df['Test Case ID'].dropna().to_list()
        embeddings = model.encode(testcases.to_list(), show_progress_bar=True)
        return 'created' 
    @staticmethod
    def doc_vec(doc):
        doc_vec=model.encode(doc)
        return doc_vec

    @staticmethod
    def model_search(doc,topn=15):
        vec = Model.doc_vec(doc)
        probalilitys = cosine_similarity(embeddings,[vec])
        top_prob = probalilitys.flatten().argsort()[-topn:][::-1]
        doc = []
        for j in top_prob:
            doc.append({'test_case_id': str(testcasesid[j]),'desc':testcases[j][0],'relevance':j})

        return doc '''



            

        

        

        




