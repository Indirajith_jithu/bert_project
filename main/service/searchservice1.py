from main.db import mongo
# from main import JSONEncoder

import os
from sentence_transformers import SentenceTransformer, models
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
#import torch

from werkzeug.utils import secure_filename
from pathlib import Path
from flask import send_file, send_from_directory

ALLOWED_EXTENSIONS = set(['csv', 'xlsx', 'xls'])
FILE_UPLOAD_FOLDER = 'main/data/testcases'

class SearchService:

    @staticmethod
    def allowed_file(filename):
        return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

    @staticmethod
    def upload_file(file):
        if file.filename == '':
            return {'msg': 'No file selected for uploading'}, 400
        if file and SearchService.allowed_file(file.filename):
            filename = secure_filename(file.filename)
            Path(FILE_UPLOAD_FOLDER).mkdir(parents=True, exist_ok=True)
            file_path = os.path.join(FILE_UPLOAD_FOLDER, filename)
            file.save(file_path)
            return {'msg': 'File successfully uploaded', 'file_path': os.path.relpath(file_path), 'file_name': filename, 'file_format': filename.rsplit('.', 1)[1].lower()}, 201
        else:
            return {'msg': 'Allowed file formats are csv, xlsx and xls'}, 400

    @staticmethod
    def get_data(file_path, file_format, Sheet_name='Test Cases'):
        if file_format == 'csv':
            df = pd.read_csv(file_path)
        else:
            df = pd.read_excel(file_path,sheet_name=Sheet_name)
        testcase_id = df['Test Case ID'].dropna().to_list()
        testcase_desc = df['Test Case Description'].dropna().to_list()
        return testcase_id, testcase_desc

    @staticmethod
    def get_embedding(testcase_desc):
        model = SearchService.get_model()
        return model.encode(testcase_desc, show_progress_bar=True)

    @staticmethod
    def get_model(path = 'main/models/model1'):
        word_embedding_model = models.Transformer(os.path.abspath(path), max_seq_length=512)
        pooling_model = models.Pooling(word_embedding_model.get_word_embedding_dimension())
        return SentenceTransformer(modules=[word_embedding_model, pooling_model])

    @staticmethod
    def add_db(testcase_id, testcase_desc, embedding):

        try:
            mongo.db["DataSource"].insert_many([{"testcase_id": testcase_id[i], "testcase_desc": testcase_desc[i], "embedding": embedding[i].tolist()} for i in range(len(testcase_id))])
            return {"msg": "Success"}, 200
        except Exception as e:
            print(e)
            return {"msg": "failed to add mongo"}, 400


    @staticmethod
    def compare(query_vec,embedded,topn):
        relavences =  cosine_similarity([query_vec],embedded)[0]
        topn_indexs = relavences.argsort()[-topn:][::-1]
        return topn_indexs, relavences[topn_indexs]

    @staticmethod
    def get_result(Objectsids,relavences):
        myquery = { "_id": { "$in": Objectsids } }
        curser =mongo.db['DataSource'].find(myquery, {"_id": 0, "testcase_desc": 1,'testcase_id':1})
        result = []
        for i ,tescase in curser:
            tescase['relavence']=  round(relavences[i], 2)
            result.append(tescase)

        return result    

    @staticmethod
    def search(query,topn=10):
        query_vec = SearchService.get_embedding(query)
        embeddings = []
        Objectsids = []
        for curser in mongo.db['DataSource'].find({}, {"_id": 1, "embedding": 1}):
             embeddings.append(curser['embedding'])
             Objectsids.append(curser['_id'])
        topn_relavences_idx,relavences =  SearchService.compare(query_vec,embeddings,topn)
        objectidslist = [Objectsids[x] for x in topn_relavences_idx]
        return SearchService.get_result(objectidslist,relavences)

 





    

    '''@staticmethod
    def search(query,topn=10):
        query_vec = SearchService.get_embedding(query)
        dic = {}
        for curser in mongo.db['DataSource'].find():
             relavance = SearchService.compare(query_vec,curser['embedding'])
             dic[relavance] = curser['testcase_id']
        top = sorted(dic.keys())[:topn]
        doc = []
        for 
            doc.append({'test_case_id': str(testcasesid[j]),'desc':testcases[j][0],'relevance':j})

        return doc'''   

