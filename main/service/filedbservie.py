from pathlib import Path
from werkzeug.utils import secure_filename
import os
import pandas as pd
from main.db import mongo

ALLOWED_EXTENSIONS = set(['csv', 'xlsx', 'xls'])
FILE_UPLOAD_FOLDER = 'main/data/testcases'


class FileDbService():


    @staticmethod
    def allowed_file(filename):
        return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

    @staticmethod
    def upload_file(file):
        if file.filename == '':
            return {'msg': 'No file selected for uploading'}, 400
        if file and FileDbService.allowed_file(file.filename):
            filename = secure_filename(file.filename)
            Path(FILE_UPLOAD_FOLDER).mkdir(parents=True, exist_ok=True)
            file_path = os.path.join(FILE_UPLOAD_FOLDER, filename)
            file.save(file_path)
            return {'msg': 'File successfully uploaded', 'file_path': os.path.relpath(file_path), 'file_name': filename, 'file_format': filename.rsplit('.', 1)[1].lower()}, 201
        else:
            return {'msg': 'Allowed file formats are csv, xlsx and xls'}, 400    

    @staticmethod
    def get_data(file_path, file_format, Sheet_name='Test Cases'):
        try:
            if file_format == 'csv':
                df = pd.read_csv(file_path)
            else:
                df = pd.read_excel(file_path,sheet_name=Sheet_name)
            testcase_id = df['Test Case ID'].dropna().to_list()
            testcase_desc = df['Test Case Description'].dropna().to_list()
            return testcase_id, testcase_desc
        except Exception as e:
            print(e)
            return {'msg':'fail to load dataset'}    


    @staticmethod
    def add_db(testcase_id, testcase_desc, embedding):

        try:
            mongo.db["DataSource"].insert_many([{"testcase_id": testcase_id[i], "testcase_desc": testcase_desc[i], "embedding": embedding[i].tolist()} for i in range(len(testcase_id))])
            return {"msg": "Success"}, 200
        except Exception as e:
            print(e)
            return {"msg": "failed to add mongo"}, 400
    @staticmethod
    def get_db_embeddings(filters,query={}): 
        return mongo.db['DataSource'].find(query,filters)