from main.controller.searchtestcontroller import SearchTestController


def initialize_routes(api):
    api.add_resource(SearchTestController,'/api/search')
    