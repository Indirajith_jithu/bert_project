from main.routes import initialize_routes
#from main.service.model import Model
from main.db import initialize_db
from main import config
from flask_cors import CORS

import json
import logging
import datetime
import numpy as np
from bson.objectid import ObjectId
from flask import Flask
from flask_restful import Api

class JSONEncoder(json.JSONEncoder):
    """ extend json-encoder class"""

    def default(self, obj):
        if isinstance(obj, ObjectId):
            return str(obj)
        elif isinstance(obj, datetime.datetime):
            return str(obj)
        elif isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(JSONEncoder, self).default(obj)

def create_app(environment):
    config_map = {
        'development': config.Development(),
        'testing': config.Testing(),
        'production': config.Production(),
    }
    logging.info("%s environment started", environment)
    config_obj = config_map[environment.lower()]

    app=Flask(__name__)
    app.config.from_object(config_obj)
    
    initialize_db(app)
    CORS(app)
    app.json_encoder = JSONEncoder
    api = Api(app)

    initialize_routes(api)
    # print(Model.model_inilization())
    return app

