# from main.service.model import Model
from main.service.searchservice import SearchService
from main.service.filedbservie import FileDbService
from main.service.modelservice import ModelService

from flask import request
from flask_restful import Resource

class SearchTestController(Resource):


    @staticmethod
    def get():
        if request.args.get('query', None):
            query = request.args.get('query', None)
            topn = int(request.args.get('topn', 10))
            return SearchService.search(query, topn)

    @staticmethod
    def post():#required file and model_type
        
        try:
            if 'file' in request.files:
                file = request.files['file']
                update_model = request.args.get('update_model', None)
                response =  FileDbService.upload_file(file)

                if response[1] == 201:
                    if response[0].get("file_path", None) is not None:
                        path = response[0].get("file_path")
                        file_format = response[0].get("file_format")
                        test_case_id, test_case_desc = FileDbService.get_data(path, file_format)
                        embedding = ModelService.get_embeddings(testcase_desc = test_case_desc,update_model = update_model)
                        return FileDbService.add_db(test_case_id, test_case_desc, embedding)
        except Exception as e:
            print(e)
            return {"message": "failed"}, 200