class Development(object):
    DEBUG = True

    MONGO_URI = 'mongodb://localhost:27017/bert_search?authSource=admin'


class Testing(object):
    DEBUG = False

    MONGO_URI = 'mongodb://localhost:27017/testcase-selector-test?authSource=admin'


class Production(object):
    DEBUG = False

    MONGO_URI = 'mongodb://user:password@localhost:27017/testcase-selector-prod?authSource=admin'
